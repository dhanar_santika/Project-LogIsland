# Project Log Island

[![pipeline status](https://gitlab.com/dhanar_santika/Project-LogIsland/badges/master/pipeline.svg)](https://gitlab.com/dhanar_santika/Project-LogIsland/commits/master)
[![coverage report](https://gitlab.com/dhanar_santika/Project-LogIsland/badges/master/coverage.svg)](https://gitlab.com/dhanar_santika/Project-LogIsland/commits/master)

Developed by @dhanar_santika in order to fulfill Batak Cina group project.

## Links to the apps?

[http://logisland.herokuapp.com](http://logisland.herokuapp.com)

## How to use it?

You can go to the link above and play normally. If something wrong occured, like some calculation error or something, you can reset the apps with thi [http://logisland.herokuapp.com/reset](http://logisland.herokuapp.com/reset)

## How do i deploy this apps?

Currently, i don't know how to automate the deployment using gitlab ci. So if you wish to deploy the apps, you may want to read the docs here `https://devcenter.heroku.com/articles/deploying-spring-boot-apps-to-heroku`