package GameController;

import RestWebController.Pemain;
import RestWebController.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONString;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

@Controller
public class GameController {

    private int currentPeriode() {
        String JSONString = getJSONString("http://logisland.herokuapp.com/api/permainan/getperiode");
        JSONObject obj = new JSONObject(JSONString);
        int periode = Integer.parseInt(obj.get("data").toString());
        return periode;
    }

    @GetMapping("/permainan")
    public String permainan(@RequestParam(name = "jumlah", required = false, defaultValue = "")
                                        String jumlah, Model model) {
        String title = "Log Island";
        model.addAttribute("title", title);
        if(jumlah.equals("")) {
            return "permainan";
        } else {
            model.addAttribute("angka", jumlah);
            return "permainan_masuk";
        }
    }

    @GetMapping("/pemain")
    public String pemain(@RequestParam(name = "nama", required = false, defaultValue = "")
                                     String nama, Model model) {
        String title = "Log Island";
        model.addAttribute("title", title);
        if(nama.equals("")) {
            return "pemain";
        } else {
            model.addAttribute("nama", nama);
            return "pemain_masuk";
        }
    }

    @GetMapping("/pemain/masuk")
    public String pemainMasuk(@RequestParam(name = "nama", required = false, defaultValue = "")
                                          String nama, HttpSession session, Model model) {
        String title = "Log Island";
        model.addAttribute("title",title);
        model.addAttribute("nama",nama);
        session.setAttribute("nama", nama);
        return "pemain_masuk";
    }

    @GetMapping("/permainan/mulai")
    public String permainanMulai(Model model) {
        String title = "Log Island";
        String JSONString = getJSONString("http://logisland.herokuapp.com/api/pemain/fix");
        JSONObject obj = new JSONObject(JSONString);
        JSONArray arr = obj.getJSONArray("data");
        int arrLength = arr.length();
        String[] kumpulanPemain = new String[arrLength];
        for(int i = 0; i<arrLength; i++) {
            JSONObject pemain = arr.getJSONObject(i);
            kumpulanPemain[i] = pemain.get("nama").toString();
        }
        model.addAttribute("title", title);
        model.addAttribute("kumpulanPemain",kumpulanPemain);
        return "permainan_mulai";
    }

    @GetMapping("/permainan/mulai/demand")
    public String permainanDemand(Model model) {
        String title = "Log Island";
        model.addAttribute("title", title);
        Random random = new Random();
        String chanceCard = "/img/card/chance" + (random.nextInt(4) + 1) + ".png";
        model.addAttribute("chanceCard", chanceCard);
        String riskCard = "/img/card/risk" + (random.nextInt(9) + 1) + ".png";
        model.addAttribute("riskCard", riskCard);
        int periode = currentPeriode();
        if(periode != 6) {
            model.addAttribute("periode", periode);
            return "permainan_mulai_demand";
        } else {
            String JSONString = getJSONString("http://logisland.herokuapp.com/api/pemain/fix");
            JSONObject obj = new JSONObject(JSONString);
            JSONArray arr = obj.getJSONArray("data");
            int arrLength = arr.length();
            String[] kumpulanHasil = new String[arrLength];
            for(int i = 0; i<arrLength; i++) {
                JSONObject pemain = arr.getJSONObject(i);
                kumpulanHasil[i] = pemain.get("nama").toString() + " - " +
                        pemain.get("skor").toString();
            }
            model.addAttribute("kumpulanHasil",kumpulanHasil);
            return "permainan_selesai";
        }
    }

    @GetMapping("/permainan/mulai/menunggu")
    public String permainanMenungguDemand(Model model) {
        String title = "Log Island";
        model.addAttribute("title", title);
        int periode = currentPeriode();
        model.addAttribute("periode",periode);
        return "permainan_mulai_menunggu";
    }

    @GetMapping("/pemain/mulai")
    public String pemainMulai(Model model, HttpSession session) {
        String nama = session.getAttribute("nama").toString();
        model.addAttribute("nama",nama);
        String JSONString = getJSONString("http://logisland.herokuapp.com/api/pemain/fix");
        JSONObject obj = new JSONObject(JSONString);
        JSONArray arr = obj.getJSONArray("data");
        for(int i=0; i<arr.length(); i++) {
            JSONObject getObj = arr.getJSONObject(i);
            if(getObj.get("nama").equals(nama)) {
                model.addAttribute("urutan", getObj.get("urutan"));
                break;
            }
        }
        return "pemain_mulai";
    }

    @GetMapping("/pemain/mulai/demand")
    public String pemainMulaiDemand(Model model, HttpSession session) {
        String title = "Log Island";
        String nama = session.getAttribute("nama").toString();
        model.addAttribute("title", title);
        model.addAttribute("nama", nama);
        int periode = currentPeriode();
        if(periode != 6) {
            model.addAttribute("periode", periode);
            return "pemain_mulai_demand";
        } else {
            String JSONString = getJSONString("http://logisland.herokuapp.com/api/pemain/fix");
            JSONObject obj = new JSONObject(JSONString);
            JSONArray arr = obj.getJSONArray("data");
            int arrLength = arr.length();
            String hasil = null;
            for(int i = 0; i<arrLength; i++) {
                JSONObject pemain = arr.getJSONObject(i);
                if(pemain.get("nama").equals(nama)) {
                    hasil = pemain.get("skor").toString();
                    model.addAttribute("hasil",hasil);
                    break;
                }
            }
            return "pemain_selesai";
        }
    }

    @GetMapping("/pemain/mulai/menunggu")
    public String pemainMenungguDemand(Model model, HttpSession session) {
        String title = "Log Island";
        String nama = session.getAttribute("nama").toString();
        model.addAttribute("title", title);
        model.addAttribute("nama", nama);
        int periode = currentPeriode();
        model.addAttribute("periode",periode);
        return "pemain_menunggu_demand";
    }

    @GetMapping("/pemain/mulai/input")
    public String pemainInputDemand(Model model, HttpSession session) {
        String title = "Log Island";
        String nama = session.getAttribute("nama").toString();
        model.addAttribute("title", title);
        model.addAttribute("nama", nama);
        int periode = currentPeriode();
        model.addAttribute("periode",periode);
        return "pemain_input_demand";
    }

    @GetMapping("/pemain/mulai/periodeberikut")
    public String pemainPeriodeBerikut(Model model, HttpSession session) {
        String title = "Log Island";
        String nama = session.getAttribute("nama").toString();
        model.addAttribute("title", title);
        model.addAttribute("nama", nama);
        int periode = currentPeriode();
        model.addAttribute("periode",periode);
        return "pemain_periode_berikut_demand";
    }

    private String getJSONString(String url) {
        String JSONString = null;
        try {
            JSONString = getJSON(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONString;
    }

    // HTTP GET request
    private String getJSON(String url) throws Exception {

        final String USER_AGENT = "Mozilla/5.0";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        return response.toString();

    }
}
