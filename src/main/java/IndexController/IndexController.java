package IndexController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping("/")
    public String index(Model model) {
        String title = "Home";
        model.addAttribute("title",title);
        return "index";
    }

    @GetMapping("/instruksi")
    public String instruksi(Model model) {
        String title = "Instruksi Permainan";
        model.addAttribute("title", title);
        return "instruksi";
    }

    @GetMapping("/reset")
    public String reset(Model model) {
        String title = "Log Island";
        model.addAttribute("title", title);
        return "reset";
    }

}
