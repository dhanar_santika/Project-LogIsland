package RestWebController;

import org.springframework.web.bind.annotation.*;
import sun.security.util.Pem;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/")
public class RestWebController {

    List<Pemain> listTmpPemain = new ArrayList<>();
    List<Pemain> listPemain = new ArrayList<>();
    Integer jumlah = 0;
    Boolean mulai = false;
    Integer pemainSelesai = 0;
    Integer periode = 1;

    @DeleteMapping("/reset")
    public Response reset() {
        listPemain = new ArrayList<>();
        listTmpPemain = new ArrayList<>();
        jumlah = 0;
        mulai = false;
        pemainSelesai = 0;
        periode = 1;
        Response response = new Response("Done",null);
        return response;
    }

    @GetMapping("/pemain/all")
    public Response getPemain() {
        Response response = new Response("Done", listTmpPemain);
        return response;
    }

    @GetMapping("/pemain/fix")
    public Response getPemainFix() {
        Response response = new Response("Done", listPemain);
        return response;
    }

    @PostMapping("/pemain/daftar")
    public Response postPemain(@RequestBody Pemain pemain) {
        listTmpPemain.add(pemain);
        Response response = new Response("Done", pemain);
        System.out.println(response.getData().toString());
        return response;
    }

    @DeleteMapping("/pemain/delete")
    public Response deleteTmpPemain() {
        listPemain = new ArrayList<>(listTmpPemain);
        listTmpPemain.clear();
        Response response = new Response("Done", listTmpPemain);
        return response;
    }

    @GetMapping("/pemain/getjumlah")
    public Response getJumlahPemain() {
        Response response = new Response("Done",jumlah);
        return response;
    }

    @PostMapping("/pemain/jumlah")
    public Response postJumlahPemain(@RequestBody String n) {
        n = n.replace("\"","");
        int jumlah = Integer.parseInt(n);
        this.jumlah = jumlah;
        Response response = new Response("Done",jumlah);
        return response;
    }

    @DeleteMapping("/pemain/deleteJumlah")
    public Response deleteJumlahPemain() {
        jumlah = 0;
        Response response = new Response("Done", jumlah);
        return response;
    }

    @GetMapping("/permainan/cekMulai")
    public Response getCekMulai() {
        Response response = new Response("Done",mulai);
        return response;
    }

    @PostMapping("/permainan/mulai")
    public Response postMulaiMain(@RequestBody Boolean b) {
        mulai = b;
        Collections.shuffle(listPemain);
        for(int i=0; i<listPemain.size(); i++) {
            listPemain.get(i).setUrutan(String.valueOf(i+1));
        }
        Response response = new Response("Done", mulai);
        return response;
    }

    @DeleteMapping("/permainan/deleteMulai")
    public Response deleteMulaiMain() {
        mulai = false;
        Response response = new Response("Done", mulai);
        return response;
    }

    @GetMapping("/permainan/getperiode")
    public Response getPeriode() {
        Response response = new Response("Done", periode);
        return response;
    }

    @PostMapping("/permainan/changeperiode")
    public Response changePeriode() {
        periode++;
        pemainSelesai = 0;
        Response response = new Response("Done", periode);
        return response;
    }

    @GetMapping("/permainan/getgiliranselesai")
    public Response getGiliranSelesai() {
        Response response = new Response("Done", pemainSelesai);
        return response;
    }

    @PostMapping("/permainan/giliranselesai")
    public Response setGiliranSelesai() {
        pemainSelesai++;
        Response response = new Response("Done", pemainSelesai);
        return response;
    }

    @PostMapping("/pemain/inputprofit")
    public Response setProfit(@RequestBody Map<String,String> input) {
        String nama = input.get("nama");
        long skor = Long.parseLong(input.get("skor"));
        for(Pemain pemain : listPemain) {
            if(pemain.getNama().equals(nama)) {
                pemain.setSkor(skor);
                Response response = new Response("Done", pemain);
                return response;
            }
        }
        return new Response("Null",null);
    }

}
