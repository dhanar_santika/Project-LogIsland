package RestWebController;

public class Response {
    private String status;
    private Object data;

    public Response() {

    }

    public Response(String status, Object data){
        this.status = status;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public Object getData() {
        return data;
    }

}