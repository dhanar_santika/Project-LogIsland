package RestWebController;

public class Pemain {
    private String nama;
    private String urutan;
    private long skor;

    public Pemain() {

    }

    public Pemain(String nama, String urutan) {
        this.nama = nama;
        this.urutan = urutan;
        this.skor = 0;
    }

    public void setNama (String nama) { this.nama = nama; }

    public String getNama () { return this.nama; }

    public void setUrutan (String urutan) { this.urutan = urutan; }

    public String getUrutan() { return this.urutan; }

    public void setSkor (long skor) { this.skor += skor; }

    public long getSkor () { return this.skor; }
}
