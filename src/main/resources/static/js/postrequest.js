// SUBMIT FORM
$("#formPemain").submit(function(event) {
    // Prevent the form from submitting via the browser.
    event.preventDefault();
    ajaxPostPemain();
});

$("#formJumlahPemain").submit(function(event) {
    // Prevent the form from submitting via the browser.
    event.preventDefault();
    ajaxPostJumlahPemain();
});

function ajaxPostProfit(){

    // PREPARE FORM DATA
    var formData = {
        "nama" : $("#nama-profit").val(),
        "skor" : $("#profit").val()
    };

    console.log(JSON.stringify(formData))

    // DO POST
    $.ajax({
        type : "POST",
        contentType : "application/json",
        url : "/api/pemain/inputprofit",
        data : JSON.stringify(formData),
        dataType : 'json',
        success : function(result) {
            if(result.status == "Done"){
                console.log(result.data);
                window.location = "/pemain/mulai/periodeberikut";
            }else{
                console.log("fail")
            }
            console.log(result);
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });

    // Reset FormData after Posting
}

function ajaxPostPemain(){

    // PREPARE FORM DATA
    var formData = {
        "nama" : $("#nama").val(),
        "urutan" : "0"
    };

    console.log(JSON.stringify(formData))

    // DO POST
    $.ajax({
        type : "POST",
        contentType : "application/json",
        url : "/api/pemain/daftar",
        data : JSON.stringify(formData),
        dataType : 'json',
        success : function(result) {
            if(result.status == "Done"){
                var nama = $("#nama").val();
                $("#nama").val("");
                console.log(result.data);
                window.location = "/pemain/masuk?nama=" + nama;
            }else{
                console.log("fail")
            }
            console.log(result);
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });

    // Reset FormData after Posting
}

function ajaxPostJumlahPemain(){

    // PREPARE FORM DATA
    var formData = $("#jumlah").val();

    console.log(JSON.stringify(formData));

    // DO POST
    $.ajax({
        type : "POST",
        contentType : "application/json",
        url : "/api/pemain/jumlah",
        data : JSON.stringify(formData),
        dataType : 'json',
        success : function(result) {
            if(result.status == "Done"){
                window.location = "/permainan?jumlah="+formData;
            }else{
                console.log("fail")
            }
            console.log(result);
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });
}

function ajaxPostMulaiMain() {
    // PREPARE FORM DATA
    var formData = true;

    console.log(JSON.stringify(formData));

    // DO POST
    $.ajax({
        type : "POST",
        contentType : "application/json",
        url : "/api/permainan/mulai",
        data : JSON.stringify(formData),
        dataType : 'json',
        success : function(result) {
            if(result.status == "Done"){
                console.log("Success ",result)
            }else{
                console.log("fail")
            }
            console.log(result);
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });
}

function endTurn() {
    $.ajax({
        type : "POST",
        contentType : "application/json",
        url : "/api/permainan/giliranselesai",
        success : function(result) {
            if(result.status == "Done"){
                console.log("Success ",result)
                window.location = "/pemain/mulai/menunggu"
            }else{
                console.log("fail")
            }
            console.log(result);
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });
}

function nextPeriodeDemand() {
    $.ajax({
        type : "POST",
        contentType : "application/json",
        url : "/api/permainan/changeperiode",
        success: function(result){
            if(result.status == "Done"){
                window.location = '/permainan/mulai/demand';
            }else{
                console.log("Fail: ", result);
            }
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });
}