// GET REQUEST
$("#getAllPemainId").click(function(event){
    event.preventDefault();
    console.log("masuk");
    ajaxGetPemain();
});

// DO GET
function ajaxGetPemain(){
    $.ajax({
        type : "GET",
        url : "/api/pemain/all",
        success: function(result){
            if(result.status == "Done"){
                $('#getPemainDiv ul').empty();
                var pemainList = "";
                $.each(result.data, function(i, pemain){
                    var pemain = "<li>" + pemain.nama + " memasuki ruang permainan</li>";
                    $('#getPemainDiv .list-group').append(pemain)
                });
                //console.log("Success: ", result);
            }else{
                console.log("Fail: ", result);
            }
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });
}

// DO GET
function check(){
    $.ajax({
        type : "GET",
        url : "/api/pemain/getjumlah",
        success: function(result){
            if(result.status == "Done"){
                var jumlah = result.data;
                setHTMLPage(jumlah)
            }else{
                console.log("Fail: ", result);
            }
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });
}

function setHTMLPage(x) {
    var jumlah = x;
    var total = $("#getPemainDiv ul li").length;
    if (total == jumlah) {
        deletePemain()
        window.location = "/permainan/mulai";
    }
}

function checkPemainDemand(){
    $.ajax({
        type : "GET",
        url : "/api/pemain/getjumlah",
        success: function(result){
            if(result.status == "Done"){
                var jumlah = result.data;
                setHTMLPageDemand(jumlah)
            }else{
                console.log("Fail: ", result);
            }
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });
}

function setHTMLPageDemand(x) {
    var jumlah = x;
    var total = $("#pemain-selesai-p").text().trim().charAt(0);
    console.log(total);
    console.log(jumlah);
    console.log(total==jumlah);
    if (total == jumlah) {
        window.location = "/permainan/mulai/menunggu";
    }
}

function getCekMulai() {
    $.ajax({
        type : "GET",
        url : "/api/permainan/cekMulai",
        success: function(result){
            if(result.status == "Done"){
                var boolean = result.data;
                if(boolean==true) {
                    console.log("hore");
                    window.location = '/pemain/mulai';
                }
            }else{
                console.log("Fail: ", result);
            }
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });
}

function getCekMulaiDemand() {
    $.ajax({
        type : "GET",
        url : "/api/permainan/cekMulai",
        success: function(result){
            if(result.status == "Done"){
                var boolean = result.data;
                if(boolean==true) {
                    console.log("hore");
                    window.location = '/pemain/mulai/input';
                }
            }else{
                console.log("Fail: ", result);
            }
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });
}

function getCekMulaiNextDemand() {
    $.ajax({
        type : "GET",
        url : "/api/permainan/cekMulai",
        success: function(result){
            if(result.status == "Done"){
                var boolean = result.data;
                if(boolean==false) {
                    console.log("hore");
                    window.location = '/pemain/mulai/demand';
                }
            }else{
                console.log("Fail: ", result);
            }
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });
}

function getMulaiDemand() {
    $.ajax({
        type : "GET",
        url : "/api/permainan/cekMulai",
        success: function(result){
            if(result.status == "Done"){
                var boolean = result.data;
                if(boolean==false) {
                    console.log("hore");
                    window.location = '/pemain/mulai/demand';
                }
            }else{
                console.log("Fail: ", result);
            }
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });
}

function ajaxGetDemand(){
    $.ajax({
        type : "GET",
        url : "/api/permainan/getgiliranselesai",
        success: function(result){
            if(result.status == "Done"){
                if(result.data>0) {
                    $('#pemain-selesai-p').html(result.data + " pemain telah mengakhiri gilirannya.");
                }
                //console.log("Success: ", result);
            }else{
                console.log("Fail: ", result);
            }
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });
}