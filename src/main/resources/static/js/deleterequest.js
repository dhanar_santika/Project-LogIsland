function deletePemain() {
    $.ajax({
        type : "DELETE",
        url : "/api/pemain/delete",
        success: function(result){
            if(result.status == "Done"){
                console.log("Success: ", result);
            }else{
                console.log("Fail: ", result);
            }
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });
}

function deleteMasukDemand() {
    $.ajax({
        type : "DELETE",
        url : "/api/permainan/deleteMulai",
        success: function(result){
            if(result.status == "Done"){
                console.log("Success: ", result);
            }else{
                console.log("Fail: ", result);
            }
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });
}

function restartGame() {
    $.ajax({
        type : "DELETE",
        url : "/api/reset",
        success: function(result){
            if(result.status == "Done"){
                console.log("Success: ", result);
                window.location = "/";
            }else{
                console.log("Fail: ", result);
            }
        },
        error : function(e) {
            console.log("ERROR: ", e);
        }
    });
}