import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import RestWebController.Pemain;
import RestWebController.Response;
import RestWebController.RestWebController;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = Application.class)
@WebMvcTest
public class ApplicationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void homepage() throws Exception {
        mockMvc.perform(get("/")).
                andExpect(content().string(containsString("Mulai")));
    }

    @Test
    public void halamanInstruksi() throws Exception {
        mockMvc.perform(get("/instruksi")).
                andExpect(content().string(containsString("Petunjuk Permainan")));
    }

    @Test
    public void halamanPermainanDropDown() throws Exception {
        mockMvc.perform(get("/permainan")).
                andExpect(content().string(containsString("Berapa orang yang akan bermain?")));
    }

    @Test
    public void halamanPermainanSelected() throws Exception {
        mockMvc.perform(get("/permainan?jumlah=2")).
                andExpect(content().string(containsString("2 orang yang akan bermain")));
    }

    @Test
    public void halamanPemain() throws Exception {
        mockMvc.perform(get("/pemain")).
                andExpect(content().string(containsString("Masukkan nama anda")));
    }

    @Test
    public void halamanPemainMenunggu() throws Exception {
        mockMvc.perform(get("/pemain?nama=Budi")).
                andExpect(content().string(containsString("Budi, silahkan menunggu pemain yang lain")));
        mockMvc.perform(get("/pemain/masuk?nama=Budi")).
                andExpect(content().string(containsString("Budi, silahkan menunggu pemain yang lain")));
    }

    /*@Test
    public void halamanPermainanMulai() throws Exception {
        RestWebController controller = new RestWebController();
        Pemain pemain1 = new Pemain("Budi","0");
        Pemain pemain2 = new Pemain("Anton","0");
        controller.addList(pemain1);
        controller.addList(pemain2);
        mockMvc.perform(get("/permainan/mulai"));
        controller.clearList();
    }*/

    @Test
    public void makeDummyResponse() {
        Response response = new Response();
    }

    @Test
    public void mockRest() throws Exception {
        mockMvc.perform(get("/api/pemain/getjumlah"))
                .andExpect(status().is2xxSuccessful());
        mockMvc.perform(get("/api/pemain/fix"))
                .andExpect(status().is2xxSuccessful());
        mockMvc.perform(get("/api/permainan/cekMulai"))
                .andExpect(status().is2xxSuccessful());
        mockMvc.perform(post("/api/pemain/jumlah")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(2)))
                .andExpect(status().is2xxSuccessful());
        mockMvc.perform(post("/api/permainan/mulai")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(true)))
                .andExpect(status().is2xxSuccessful());
        mockMvc.perform(delete("/api/pemain/delete"))
                .andExpect(status().is2xxSuccessful());
        mockMvc.perform(delete("/api/pemain/deleteJumlah"))
                .andExpect(status().is2xxSuccessful());
        mockMvc.perform(delete("/api/permainan/deleteMulai"))
                .andExpect(status().is2xxSuccessful());
    }

    /*@Test
    public void halamanPemainMulai() throws Exception {
        Map<String, Object> sessionattr = new HashMap<>();
        sessionattr.put("nama", "Budi");

        mockMvc.perform(get("/pemain/mulai").
                sessionAttrs(sessionattr).param("nama", "Budi"));
    }*/

    @Test
    public void daftarPemain() throws Exception {
        Pemain pemain = new Pemain("Budi","0");
        mockMvc.perform(post("/api/pemain/daftar")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(pemain)));
        mockMvc.perform(get("/api/pemain/all"))
                .andExpect(content().string(containsString("Budi")));

    }

}
